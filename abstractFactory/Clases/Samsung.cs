﻿using System;
using abstractFactory.Interfaces;

namespace abstractFactory.Clases
{
    public class Samsung: IMobilePhone
    {
        public Samsung()
        {
        }

        public INormalPhone GetNormalPhone()
        {
            return new SamsungGuru();
        }

        public ISmartPhone GetSmartPhone()
        {
            return new SamsungGalaxy();
        }
    }
}
