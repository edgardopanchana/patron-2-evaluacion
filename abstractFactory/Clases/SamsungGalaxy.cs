﻿using System;
using abstractFactory.Interfaces;

namespace abstractFactory.Clases
{
    public class SamsungGalaxy: ISmartPhone
    {
        public SamsungGalaxy()
        {
        }

        public string GetModelDetails()
        {
            return "Model: Samsung Galaxy\nRAM: 2GB\nCamera: 13MP\n";
        }
    }
}
