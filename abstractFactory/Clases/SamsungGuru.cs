﻿using System;
using abstractFactory.Interfaces;

namespace abstractFactory.Clases
{
    public class SamsungGuru: INormalPhone
    {
        public SamsungGuru()
        {
        }

        public string GetModelDetails()
        {
            return "Model: Samsung Guru\nRAM: NA\nCamera: NA\n";
        }
    }
}
