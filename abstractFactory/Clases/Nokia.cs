﻿using System;
using abstractFactory.Interfaces;


namespace abstractFactory.Clases
{
    public class Nokia: IMobilePhone
    {
        public Nokia()
        {
        }

        public INormalPhone GetNormalPhone()
        {
            return new Nokia1600();
        }

        public ISmartPhone GetSmartPhone()
        {
            return new NokiaPixel();
        }
    }
}
