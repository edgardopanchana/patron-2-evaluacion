﻿using System;
using abstractFactory.Interfaces;

namespace abstractFactory.Clases
{
    public class NokiaPixel:ISmartPhone
    {
        public NokiaPixel()
        {
        }

        public string GetModelDetails()
        {
            return "Model: Nokia Pixel\nRAM: 3GB\nCamera: 8MP\n";
        }
    }
}
