﻿using System;
using abstractFactory.Interfaces;

namespace abstractFactory.Clases
{
    public class Nokia1600: INormalPhone
    {
        public Nokia1600()
        {
        }

        public string GetModelDetails()
        {
            return "Model: Nokia 1600\nRAM: NA\nCamera: NA\n";
        }
    }
}
