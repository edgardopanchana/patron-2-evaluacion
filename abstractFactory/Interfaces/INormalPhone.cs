﻿using System;
namespace abstractFactory.Interfaces
{
    public interface INormalPhone
    {
        string GetModelDetails();
    }
}
