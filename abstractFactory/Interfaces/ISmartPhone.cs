﻿using System;
namespace abstractFactory.Interfaces
{
    public interface ISmartPhone
    {
        string GetModelDetails();
    }
}
